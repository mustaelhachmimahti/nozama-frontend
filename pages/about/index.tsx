import Layout from "../../components/layouts/Layout"
import {JwtPayload, verify} from "jsonwebtoken";
import {AboutProps, PayloadResponse} from "../../interfaces";
import axios from "axios";
import {server} from "../../config/api-url";
import Link from "next/link"

const About = ({logged}: AboutProps) => {
    return (
        <Layout protect={logged} description={"This is the about us page, we talk about our site and about us."}
                title={"About"}>
            <div
                className="p-4 w-10/12 mx-auto mt-32 text-center bg-white rounded-lg border shadow-md sm:p-8 dark:bg-gray-800 dark:border-gray-700">
                <h5 className="mb-2 text-3xl font-bold text-gray-900 dark:text-white">About us</h5>
                <p className="mb-5 text-base text-gray-500 sm:text-lg dark:text-gray-400">Who are we?
                </p>
                <p>(pending final version) Nozama in France: a positive contribution to the economy We are passionate
                    about serving our customers in France. However, while we think of them first, we also attach the
                    utmost importance to our economic, social and environmental footprint at the local level. By putting
                    all our energy, know-how and innovation at the service of the French people, we contribute to the
                    growth of the French economy. We wanted to share an overview of how we contribute to the growth of
                    the French economy, the creation of thousands of jobs, the financing of public services and the
                    French social model; while taking care to preserve the environment.</p>
            </div>

            {
                !logged ? <div className="mt-10"><p className="text-center">Log in if you already have an account, <span
                    className="text-blue-600"><Link href="/login"><a>login</a></Link></span></p><br/>
                    <p className="text-center">If you dont have an account, create one for free and find what you are
                        looking for, <span
                            className="text-blue-600"><Link href="/signup"><a>signup</a></Link></span></p></div> : null
            }
        </Layout>
    )
}


export async function getServerSideProps(context: any) {
    try {
        const verifyAccessToken: JwtPayload | string = verify(context.req.cookies.jwtCookie, process.env.SECRET_JWT_TOKEN ? process.env.SECRET_JWT_TOKEN : "")
        if (verifyAccessToken) {
            const response: PayloadResponse = await axios(`${server}/auth/payload`, {
                headers: {
                    Authorization: `Bearer ${context.req.cookies.jwtCookie}`,
                },
            })
            if (JSON.stringify(response.data.payload) === JSON.stringify(verifyAccessToken)) {
                return {
                    props: {logged: true},
                }
            }
        } else {
            return {
                props: {logged: false},
            }
        }
    } catch (err: any) {
        return {
            props: {logged: false},
        }
    }

}

export default About