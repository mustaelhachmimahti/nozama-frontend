import type {NextApiRequest, NextApiResponse} from 'next'
import {serialize} from "cookie";

type Data = {
    message: string | string[]
}

export default function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
): void {
    const jwt: string = req.cookies.jwtCookie
    if (!jwt) {
        res.status(401).json({message: ['You are not logged in']})
    } else {
        const cookie: string = serialize('jwtCookie', '', {
            maxAge: 3600,
            httpOnly: true,
            secure: process.env.NODE_ENV === 'production',
            sameSite: 'strict',
            path: '/'
        })
        res.setHeader('Set-Cookie', cookie)
        res.status(200).json({message: ['Successfully logged out!']})
    }
}
