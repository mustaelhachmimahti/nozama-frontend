import type {NextApiRequest, NextApiResponse} from 'next'
import axios from "axios";
import {server} from "../../../config/api-url";
import {JwtPayload, verify} from "jsonwebtoken";
import {serialize} from "cookie";
import {PayloadResponse} from "../../../interfaces";

type Data = {
    message: string[]
}

type AccessToken = {
    accessToken: string
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
): Promise<void> {
    const {accessToken}: AccessToken = req.body

    try {
        const response: PayloadResponse = await axios(`${server}/auth/payload`, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        })
        const verifyAccessToken: JwtPayload | string = verify(accessToken, process.env.SECRET_JWT_TOKEN ? process.env.SECRET_JWT_TOKEN : "")
        if (JSON.stringify(response.data.payload) === JSON.stringify(verifyAccessToken)) {
            const cookie = serialize('jwtCookie', accessToken, {
                maxAge: 3600,
                httpOnly: true,
                secure: process.env.NODE_ENV === 'production',
                sameSite: 'strict',
                path: '/'
            })
            res.setHeader('Set-Cookie', cookie)
            res.status(200).json({message: ['OK']})
        } else {
            res.status(401).json({message: ['Invalid credentials']})
        }
    } catch (err: any) {
        console.log(err.response)
        if (err) res.status(400).json({message: ["Something went wrong! Try later"]})
    }
}
