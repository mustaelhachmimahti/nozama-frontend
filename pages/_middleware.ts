import {NextRequest, NextResponse} from "next/server";
import {routes} from "../config/api-url";
import * as jose from 'jose';

type Token = {
    [p: string]: string
}
const middleware = async (req: NextRequest): Promise<any> => {
    const token: { [p: string]: string } = req.cookies
    const url: string = req.url
    if (url.includes('/about')) {
        return NextResponse.next()
    } else if (url.includes('/admin')) {
        if (Object.keys(token).length === 0) {
            return NextResponse.redirect(`${routes}/login`)
        }
        const payload = await jose.jwtVerify(token.jwtCookie, new TextEncoder().encode(process.env.SECRET_JWT_TOKEN))
        if (payload.payload.role === "ADMIN") {
            return NextResponse.next()
        }
        return NextResponse.redirect(`${routes}/`)

    } else if (!url.includes('/login') && !url.includes('/signup')) {
        if (token === undefined) {
            return NextResponse.redirect(`${routes}/login`)
        }
        try {
            await jose.jwtVerify(token.jwtCookie, new TextEncoder().encode(process.env.SECRET_JWT_TOKEN))
            return NextResponse.next()
        } catch (err) {
            return NextResponse.redirect(`${routes}/login`)
        }
    } else {
        if (token) {
            try {
                await jose.jwtVerify(token.jwtCookie, new TextEncoder().encode(process.env.SECRET_JWT_TOKEN))
                return NextResponse.redirect(`${routes}/`)
            } catch (err) {
                return NextResponse.next()
            }
        }
    }
    return NextResponse.next()
}

export default middleware