import Layout from "../../components/layouts/Layout";
import LoginForms from "../../components/forms/LoginForm";
import {LoginDto} from "../../interfaces";
import {ChangeEvent, useState} from "react";
import {server} from "../../config/api-url";
import axios from 'axios'
import {NextPage} from "next";

const Login: NextPage = () => {
    const [error, setError] = useState<string[]>([]);

    const [loginData, setLoginData] = useState<LoginDto>({
        email: "",
        password: ""
    });

    const handleLogin = async (e: ChangeEvent): Promise<void> => {
        e.preventDefault();
        try {
            const response = await axios.post(`${server}/auth/login`, loginData)
            if (response)
                if (response.status === 200) {
                    localStorage.setItem('accessToken', response.data.response.accessToken)
                    const responseApiProxy = await axios.post("/api/auth/login", {accessToken: response.data.response.accessToken})
                    const user: any = await axios(`${server}/auth/payload`, {
                        headers: {
                            Authorization: `Bearer ${response.data.response.accessToken}`,
                        },
                    });

                    localStorage.setItem('userLogged', JSON.stringify(user.data.payload));
                    if (responseApiProxy.status) window.location.href = '/'
                }
        } catch (err: any) {
            if (err.response)
                if (err.response.status) {
                    if (err.response.status === 403) {
                        const errors: any = [err.response.data.message];
                        setError(errors);
                    }
                    if (err.response.status === 400) {
                        const errors = err.response.data.message.map((e: any) => e);
                        setError(errors);
                    }
                }
        }
    }

    return (
        <Layout protect={false} description={"This is the login page to have access to the site web."} title={"Login"}>
            <h1 className="text-center text-4xl mt-5">Login</h1>
            <div className="mt-20"><LoginForms action={handleLogin} error={error} setState={setLoginData}
                                               state={loginData}/></div>
        </Layout>
    )
}


export default Login