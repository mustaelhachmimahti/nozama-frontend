import Layout from "../../../components/layouts/Layout"

const User = () => {
    return (
        <Layout protect={true} description={"The list of all users that admin can read users"}
                title={"Users"}>

            <div className="relative overflow-x-auto shadow-md sm:rounded-lg w-9/12 mx-auto mt-28">
                <div className="flex justify-center mb-20"><p className="text-3xl">Je ne lai pas terminé parce que jai
                    réalisé que
                    je navais pas à le
                    faire.</p></div>
                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Email
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Firstname
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Lastname
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Address
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Role
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Created at
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Updated at
                        </th>
                        <th scope="col" className="px-6 py-3">
                            <span className="sr-only">Edit</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th scope="row"
                            className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                            musta@musta.com
                        </th>
                        <td className="px-6 py-4">
                            Musta
                        </td>
                        <td className="px-6 py-4">
                            Delyan
                        </td>
                        <td className="px-6 py-4">
                            Doctor ferran
                        </td>
                        <td className="px-6 py-4">
                            User
                        </td>
                        <td className="px-6 py-4">
                            27/04/2022
                        </td>
                        <td className="px-6 py-4">
                            27/04/2022
                        </td>
                        <td className="px-6 py-4 text-right">
                            <a href="#"
                               className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Edit</a>
                        </td>
                        <td className="px-6 py-4 text-right">
                            <a href="#"
                               className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Delete</a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </Layout>
    )
}

export async function getServerSideProps(context: any) {
    //todo
    // const response = await axios(`${server}/users`, {
    //     headers: {
    //         Authorization: `Bearer ${context.req.cookies.jwtCookie}`,
    //     },
    // })
    return {
        props: {data: ""},
    }
}


export default User