import axios from "axios";
import Layout from "../../../../components/layouts/Layout";
import {server} from "../../../../config/api-url";
import {Product, ProductOneProps, Tag, TagsProductsRelDto} from "../../../../interfaces";
import moment from "moment";

const OneProduct = ({data, tag}: ProductOneProps) => {
    const deleteProduct = async (id: number) => {
        const accessToken = localStorage.getItem('accessToken')
        try {
            const response = await axios.delete(`${server}/product/remove/${+id}`, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            if (response.status === 200) window.location.href = "/admin/product"
        } catch (err: any) {
            //todo
            console.log(err.response)
        }
    }
    return (
        <Layout protect={true} description={data.description} title={data.title}>
            <div
                className="p-4 w-full text-center bg-white rounded-lg border shadow-md sm:p-8 dark:bg-gray-800 dark:border-gray-700">
                <h5 className="mb-2 text-3xl font-bold text-gray-900 dark:text-white">{data.title}</h5>
                <p className="mb-5 text-base text-gray-500 sm:text-lg dark:text-gray-400">Tag: {
                    tag.length ? tag.map((e: string, i: number) => (
                        e + ", ")) : "No tags"
                }</p>
                <div className="justify-between space-y-4 sm:flex sm:space-y-0 sm:space-x-4">
                    <img alt="Placeholder" className="block h-3/6 "
                         src={data.imageLink ? data.imageLink : "https://picsum.photos/600/400/?random"}/>
                    <div>
                        <p className="text-left"><span
                            className="font-bold">Description:</span><br/> {data.description ? data.description : "No description"}
                        </p>
                        <p className="text-left"><span
                            className="font-bold">Price:</span><br/> {data.price ? data.price : "No price"}$
                        </p>
                        <p className="text-left"><span
                            className="font-bold">Stock:</span><br/> {data.stock ? data.stock : "No stock"}
                        </p>
                        <p className="text-left"><span
                            className="font-bold">Stock:</span><br/> {data.createdAt ? moment(data.createdAt).format('MMMM d, YYYY') : "No date"}
                        </p>
                        <p className="text-left font-bold">Options:</p>
                        <div className="flex">
                            <button onClick={() => deleteProduct(data.id)}
                                    className="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            >Delete
                            </button>
                            {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                            <a href={"/admin/product/update/" + data.id}
                               className="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            >Edit
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
            <div className="flex justify-center mt-10"><a href="/admin/product"
                                                          className="text-blue-500 hover:underline">{'<<'} Back
                to
                products</a></div>

        </Layout>
    )
}

export async function getServerSideProps({query, req}: any) {
    const {id} = query
    try {
        const response = await axios(`${server}/product/${id}`, {
            headers: {
                Authorization: `Bearer ${req.cookies.jwtCookie}`,
            },
        })
        const responseTag = await axios(`${server}/product/tag/all`, {
            headers: {
                Authorization: `Bearer ${req.cookies.jwtCookie}`,
            },
        })
        const tagsProduct: string[] = []
        const resAllTag: Tag[] = responseTag.data.response
        const res: Product = response.data.response
        res.tags.forEach((e: TagsProductsRelDto) => {
            return resAllTag.forEach((el: Tag) => {
                if (+e.tagId === el.id) tagsProduct.push(el.tagName)
            })
        })
        return {
            props: {data: res, tag: tagsProduct},
        }
    } catch (err) {
        return {
            props: {data: {}, tag: []},
        }
    }
}

export default OneProduct