import Layout from "../../../components/layouts/Layout";
import axios from "axios";
import {server} from "../../../config/api-url";
import {Product} from "../../../interfaces";
import {useEffect, useState} from "react";
import Card from "../../../components/card/Card";
import ReactPaginate from "react-paginate";

const Product = () => {

    const [data, setData] = useState<Product[]>([]);
    const [pageNumber, setPageNumber] = useState<number>(0)
    const [match, setMatch] = useState<Product[]>([]);
    const [search, setSearch] = useState<string>("")
    const productsPerPage: number = 10
    const pagesVisited: number = pageNumber * productsPerPage
    const displayProduct: Product[] = data.slice(pagesVisited, pagesVisited + productsPerPage)
    const pageCount = Math.ceil(data.length / productsPerPage)
    const changePage = ({selected}: any) => {
        setPageNumber(selected)
        window.scrollTo(0, 0);
    }

    const truncate = (str: string, max = 10): string => {
        const array = str.trim().split(' ');
        const ellipsis = array.length > max ? '...' : '';
        return array.slice(0, max).join(' ') + ellipsis;
    };

    const fetchProducts = async () => {
        const accessToken = localStorage.getItem('accessToken')
        try {
            const response = await axios(`${server}/product`, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            const res: Product[] = response.data.response
            setData(res.reverse())
        } catch (err: any) {
            //todo
            console.log(err.response)
        }
    }

    const handleSearch = async (e: any) => {
        const accessToken = localStorage.getItem('accessToken')
        setSearch(e.target.value)
        try {
            const response = await axios.post(`${server}/product/match/search`, {title: search}, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            const res: Product[] = response.data.response
            setMatch(res)
        } catch (err: any) {

        }
    }

    useEffect(() => {
        fetchProducts()
    }, [data])

    return (
        <>
            <Layout protect={true} description={"The list of al products that admin can edit delete and add products"}
                    title={"Products backoffice"}>
                <div className="container my-12 mx-auto px-4 md:px-12">
                    <div className="flex justify-end">
                        {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                        <a href="/admin/product/create"
                           className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Add
                            product
                        </a>
                    </div>
                    <div>
                        <label htmlFor="default-search"
                               className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-gray-300">Search</label>
                        <div className="relative">
                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none"
                                     stroke="currentColor"
                                     viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                          d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"/>
                                </svg>
                            </div>
                            <input type="search" id="default-search"
                                   className="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                   placeholder="Search..."
                                   onChange={handleSearch}
                            />
                        </div>
                    </div>
                    <div className="flex flex-wrap -mx-1 lg:-mx-4">
                        {
                            search.length !== 0 ? match.map((product: Product, i: number) => (
                                <Card key={i} id={product.id} title={product.title} description={product.description}
                                      imageLink={product.imageLink} price={product.price} stock={product.stock}
                                      createdAt={product.createdAt}
                                      updatedAt={product.updatedAt} admin={true} href={"/products/"}/>
                            )) : displayProduct.map((product: Product, i: number) => (
                                <Card key={i} id={product.id} title={product.title} description={product.description}
                                      imageLink={product.imageLink} price={product.price} stock={product.stock}
                                      createdAt={product.createdAt}
                                      updatedAt={product.updatedAt} admin={true} href={"/admin/product/see/"}/>
                            ))
                        }
                    </div>
                    {search.length === 0 ? <div className="flex justify-center mt-10">
                        <ReactPaginate className="flex items-center" previousLabel={"Previous"} nextLabel={"Next"}
                                       pageCount={pageCount}
                                       onPageChange={changePage}
                                       pageLinkClassName={"py-2 px-3 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"}
                                       previousClassName={"py-2 px-3 ml-0 leading-tight text-gray-500 bg-white rounded-l-lg border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"}
                                       nextLinkClassName={"py-2 px-3 leading-tight text-gray-500 bg-white rounded-r-lg border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"}/>
                    </div> : null}
                </div>
            </Layout>
        </>
    )
}

export default Product