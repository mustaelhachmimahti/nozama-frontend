import Layout from "../../../components/layouts/Layout"
import ProductForm from "../../../components/forms/ProductForm";
import {ChangeEvent, useState} from "react";
import {ProductDto, Tag, TagsProps} from "../../../interfaces";
import axios from "axios";
import {server} from "../../../config/api-url";

const CreateProduct = ({data}: TagsProps) => {
    const [product, setProduct] = useState<ProductDto>({
        title: '',
        description: '',
        imageLink: '',
        price: 0,
        stock: 0
    });
    const [error, setError] = useState<string[]>([]);
    const [tag] = useState<any[]>([{}])

    const handleTageValue = (e: ChangeEvent) => {
        while (tag.length > 0) {
            tag.pop();
        }
        document.querySelectorAll("#default-checkbox").forEach((e: any) => {
            if (e.checked) tag.push({tagId: +e.value})
        })
    }

    const handleAddProduct = async (e: ChangeEvent) => {
        e.preventDefault()

        const accessToken = localStorage.getItem('accessToken')
        try {
            const response = await axios.post(`${server}/product`, product, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            if (response.status === 200) {
                const idProduct = response.data.response.id
                const relationship = tag.map((e: any) => {
                    return {...e, ...{productId: idProduct}}
                })
                const result = await axios.post(`${server}/product/tag-product`, relationship, {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                })
                if (result.status === 200) window.location.href = "/admin/product"
            }

        } catch (err: any) {
            console.log(err.response)
            if (err.response) {
                if (err.response.status) {
                    if (err.response.status === 403) {
                        const errors: any = [err.response.data.message];
                        setError(errors);
                    }
                    if (err.response.status === 400) {
                        const errors = err.response.data.message.map((e: any) => e);
                        setError(errors);
                    }
                }
            }
        }
    }

    return (
        <Layout protect={true} description={"This page is where the admin can create a product."}
                title={"Create product"}>

            <div className="mt-20 w-9/12 mx-auto">
                <ProductForm
                    setState={setProduct} state={product} action={handleAddProduct} tag={data}
                    actionTag={handleTageValue} error={error}/>
            </div>
        </Layout>
    )
}

export async function getServerSideProps({req}: any) {
    try {
        const response = await axios(`${server}/product/tag/all`, {
            headers: {
                Authorization: `Bearer ${req.cookies.jwtCookie}`,
            },
        })
        const res: Tag[] = response.data.response
        return {
            props: {data: res},
        }
    } catch (err) {
        return {
            props: {data: []},
        }
    }
}


export default CreateProduct