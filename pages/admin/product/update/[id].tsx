import Layout from "../../../../components/layouts/Layout";
import ProductForm from "../../../../components/forms/ProductForm";
import {ChangeEvent, useEffect, useState} from "react";
import {Product, ProductDto, Tag, TagsProductsRelDto, UpdateProductProps} from "../../../../interfaces";
import axios from "axios";
import {server} from "../../../../config/api-url";

const UpdateProduct = ({data}: UpdateProductProps) => {
    const [updateProduct, setUpdateProduct] = useState<ProductDto>({
        title: data.resProd.title,
        description: data.resProd.description,
        imageLink: data.resProd.imageLink,
        price: +data.resProd.price,
        stock: +data.resProd.stock
    });
    const [error, setError] = useState<string[]>([]);
    const [tag] = useState<any[]>([{}])

    const handleTageValue = (e: ChangeEvent) => {
        while (tag.length > 0) {
            tag.pop();
        }
        document.querySelectorAll("#default-checkbox").forEach((e: any) => {
            if (e.checked) {
                tag.push({tagId: +e.value})
            }
        })
    }
    const fetchTags = () => {
        document.querySelectorAll("#default-checkbox").forEach((e: any) => {
            data.resProd.tags.forEach((el: TagsProductsRelDto) => {
                if (+el.tagId === +e.value) {
                    e.checked = true
                    tag.push({tagId: +e.value})
                }
            })
        })
    }

    useEffect(() => {
        fetchTags()
    }, []);


    const handleUpdateProduct = async (e: ChangeEvent) => {
        e.preventDefault()
        const accessToken = localStorage.getItem('accessToken')
        try {
            const response = await axios.patch(`${server}/product/update/${data.resProd.id}`, updateProduct, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            if (response.status === 200) {
                const idProduct = response.data.response.id
                const relationship = tag.map((e: any) => {
                    return {...e, ...{productId: idProduct}}
                })
                relationship.shift()
                const result = await axios.post(`${server}/product/tag-product`, relationship, {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                })
                if (result.status === 200) window.location.href = "/admin/product/see/" + data.resProd.id
            }
        } catch (err: any) {
            console.log(err.response)
            if (err.response) {
                if (err.response.status) {
                    if (err.response.status === 403) {
                        const errors: any = [err.response.data.message];
                        setError(errors);
                    }
                    if (err.response.status === 400) {
                        const errors = err.response.data.message.map((e: any) => e);
                        setError(errors);
                    }
                }
            }
        }
    }

    return (
        <Layout protect={true} description={"Here the admin can update a product"}
                title={"Update" + " " + data.resProd.title}>
            <div className="mt-20 w-9/12 mx-auto">
                <ProductForm state={updateProduct} setState={setUpdateProduct} action={handleUpdateProduct}
                             tag={data.res}
                             actionTag={handleTageValue} inputValue={data.resProd} error={error}/>
            </div>
        </Layout>
    )
}

export async function getServerSideProps({req, query}: any) {
    try {
        const response = await axios(`${server}/product/tag/all`, {
            headers: {
                Authorization: `Bearer ${req.cookies.jwtCookie}`,
            },
        })

        const responseProduct = await axios(`${server}/product/${query.id}`, {
            headers: {
                Authorization: `Bearer ${req.cookies.jwtCookie}`,
            },
        })
        const res: Tag[] = response.data.response
        const resProd: Product = responseProduct.data.response
        const data = {
            res, resProd
        }
        return {
            props: {data},
        }
    } catch (err) {
        return {
            props: {data: []},
        }
    }
}


export default UpdateProduct


//////////////////////

//
//
//
// import Layout from "../../../../components/layouts/Layout";
// import ProductForm from "../../../../components/forms/ProductForm";
// import {ChangeEvent, useEffect, useState} from "react";
// import {Product, ProductDto, Tag, TagsProductsRelDto, UpdateProductProps} from "../../../../interfaces";
// import axios from "axios";
// import {server} from "../../../../config/api-url";
//
// const UpdateProduct = ({data}: UpdateProductProps) => {
//     const [updateProduct, setUpdateProduct] = useState<ProductDto>({
//         title: data.resProd.title,
//         description: data.resProd.description,
//         imageLink: data.resProd.imageLink,
//         price: +data.resProd.price,
//         stock: +data.resProd.stock
//     });
//     const [error, setError] = useState<string[]>([]);
//     const [tag] = useState<any[]>([{}])
//
//     const handleTageValue = (e: ChangeEvent) => {
//         while (tag.length > 0) {
//             tag.pop();
//         }
//         document.querySelectorAll("#default-checkbox").forEach((e: any) => {
//             if (e.checked) {
//                 tag.push({tagId: +e.value})
//             }
//         })
//     }
//     const [tagArray] = useState<number[]>([])
//     const fetchTags = () => {
//         document.querySelectorAll("#default-checkbox").forEach((e: any) => {
//             data.resProd.tags.forEach((el: TagsProductsRelDto) => {
//                 if (+el.tagId === +e.value) {
//                     e.checked = true
//                     tagArray.push(+e.value)
//                 }
//             })
//         })
//         // console.log(tagArray)
//         // tagArray.forEach((e: number) => {
//         // tag.push({tagId: e})
//         // console.log(e)
//         // })
//     }
//
//     useEffect(() => {
//         fetchTags()
//     }, []);
//
//
//     const handleUpdateProduct = async (e: ChangeEvent) => {
//         e.preventDefault()
//         const accessToken = localStorage.getItem('accessToken')
//         try {
//             const response = await axios.patch(`${server}/product/update/${data.resProd.id}`, updateProduct, {
//                 headers: {
//                     Authorization: `Bearer ${accessToken}`,
//                 },
//             })
//             if (response.status === 200) {
//                 const idProduct = response.data.response.id
//                 const tagIds: any = new Set(tagArray)
//                 const uniq = [...tagIds];
//                 console.log(uniq)
//                 const relationship = uniq.map((e: any) => {
//                     return {...{tagId: e}, ...{productId: idProduct}}
//                 })
//
//                 const result = await axios.patch(`${server}/product/tag-product/update/${idProduct}`, relationship, {
//                     headers: {
//                         Authorization: `Bearer ${accessToken}`,
//                     },
//                 })
//                 // if (result.status === 200) window.location.href = "/admin/product/see/" + data.resProd.id
//             }
//         } catch (err: any) {
//             console.log(err.response)
//             if (err.response) {
//                 if (err.response.status) {
//                     if (err.response.status === 403) {
//                         const errors: any = [err.response.data.message];
//                         setError(errors);
//                     }
//                     if (err.response.status === 400) {
//                         const errors = err.response.data.message.map((e: any) => e);
//                         setError(errors);
//                     }
//                 }
//             }
//         }
//     }
//
//     return (
//         <Layout protect={true} description={"Here the admin can update a product"}
//                 title={"Update" + " " + data.resProd.title}>
//             <div className="mt-20 w-9/12 mx-auto">
//                 <ProductForm state={updateProduct} setState={setUpdateProduct} action={handleUpdateProduct}
//                              tag={data.res}
//                              actionTag={handleTageValue} inputValue={data.resProd} error={error}/>
//             </div>
//         </Layout>
//     )
// }
//
// export async function getServerSideProps({req, query}: any) {
//     try {
//         const response = await axios(`${server}/product/tag/all`, {
//             headers: {
//                 Authorization: `Bearer ${req.cookies.jwtCookie}`,
//             },
//         })
//
//         const responseProduct = await axios(`${server}/product/${query.id}`, {
//             headers: {
//                 Authorization: `Bearer ${req.cookies.jwtCookie}`,
//             },
//         })
//         const res: Tag[] = response.data.response
//         const resProd: Product = responseProduct.data.response
//         const data = {
//             res, resProd
//         }
//         return {
//             props: {data},
//         }
//     } catch (err) {
//         return {
//             props: {data: []},
//         }
//     }
// }
//
//
// export default UpdateProduct