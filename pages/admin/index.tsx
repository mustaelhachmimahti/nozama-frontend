import Layout from './../../components/layouts/Layout'
import {NextPage} from "next";

const Admin: NextPage = () => {
    return (
        <Layout protect={true} description={"The backoffice that only have access admin user"}
                title={"Backoffice"}>
            <div><h1 className="text-center text-3xl mt-20">Welcome to the backoffice</h1></div>
            <div className="flex justify-center mt-32">
                <div className="grid grid-cols-1 lg:grid-cols-3 gap-14 mb-10">

                    {/*IMPORTANT!!!!!!*/}

                    {/*Je ne peux pas utiliser ajax car j'ai utilisé un framework css avec js inclus pour gagner du temps*/}
                    {/*dans le développement de l'application, le truc c'est que si j'utilise ajax la page qui a l'en-tête*/}
                    {/*ne fait pas la requête au cdn donc quand je change de page sans recharger le js de la page suivante*/}
                    {/*ne fonctionne pas. Le commentaire au dessus des balises html <a></a> sont à ignorer pour*/}
                    {/*mettre <Link/> de next qui permet de faire ajax et de ne pas avoir d'erreurs pour pouvoir déployer*/}
                    {/*l'application.*/}


                    {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                    <a href="/admin/product"
                       className={`w-52 rounded-[30px] p-6 bg-gray-300 hover:bg-gray-400 flex flex-col justify-center align-middle items-center`}>
                        <i className={"fa-solid fa-cart-shopping text-white text-6xl mb-4"}/>
                        <p className="text-white text-lg">
                            Products
                        </p>
                    </a>
                    {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                    <a href="/admin/user"
                       className={`w-52 rounded-[30px] p-6 bg-gray-300 hover:bg-gray-400 flex flex-col justify-center align-middle items-center`}>
                        <i className={"fa-solid fa-users text-white text-6xl mb-4"}/>
                        <p className="text-white text-lg">
                            Users
                        </p>
                    </a>
                    {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                    <a href="/admin/tag"
                       className={`w-52 rounded-[30px] p-6 bg-gray-300 hover:bg-gray-400 flex flex-col justify-center align-middle items-center`}>
                        <i className={"fa-solid fa-tag text-white text-6xl mb-4"}/>
                        <p className="text-white text-lg">
                            Tags
                        </p>
                    </a>
                </div>
            </div>
        </Layout>
    )
}

export default Admin