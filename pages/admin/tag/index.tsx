import Layout from "../../../components/layouts/Layout";
import axios from "axios";
import {server} from "../../../config/api-url";
import {Tag} from "../../../interfaces";
import moment from "moment";
import {useEffect, useState} from "react";

const Tag = () => {

    const [data, setData] = useState<Tag[]>([]);
    const deleteTag = async (id: number) => {
        const accessToken = localStorage.getItem('accessToken')
        try {
            const response = await axios.delete(`${server}/product/remove/tag/${+id}`, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
        } catch (err: any) {
            //todo
            console.log(err.response)
        }
    }

    const fetchTags = async () => {
        const accessToken = localStorage.getItem('accessToken')
        try {
            const responseTag = await axios(`${server}/product/tag/all`, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            const tags: Tag[] = responseTag.data.response
            setData(tags.reverse())
        } catch (err: any) {
            //todo
            console.log(err.response)
        }
    }


    useEffect(() => {
        fetchTags()
    }, [data]);


    return (
        <Layout protect={true} description={"The list of all tags that admin can edit delete and add tags"}
                title={"Tags"}>
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg w-9/12 mx-auto mt-28">
                <div className="flex justify-end mb-3">
                    {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                    <a href="/admin/tag/create"
                       className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Add
                        product
                    </a>
                </div>
                <table className="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" className="px-6 py-3">
                            Name
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Created at
                        </th>
                        <th scope="col" className="px-6 py-3">
                            Updated at
                        </th>
                        <th scope="col" className="px-6 py-3">
                            <span className="sr-only">Edit</span>
                        </th>
                        <th scope="col" className="px-6 py-3">
                            <span className="sr-only">Delete</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        data.length ?
                            data.map((e: Tag, i: number) => (
                                <tr key={i} className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <td scope="row"
                                        className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                        {e.tagName ? e.tagName : "No name"}
                                    </td>
                                    <td className="px-6 py-4">
                                        {e.createdAt ? moment(e.createdAt).format('MMMM d, YYYY') : "No date"}
                                    </td>
                                    <td className="px-6 py-4">
                                        {e.createdAt ? moment(e.updatedAt).format('MMMM d, YYYY') : "No date"}
                                    </td>
                                    <td className="px-6 py-4 text-right">
                                        <a href={"/admin/tag/update/" + e.id}
                                           className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Edit
                                        </a>
                                    </td>
                                    <td className="px-6 py-4 text-right">
                                        <button type="button" onClick={() => deleteTag(e.id)}
                                                className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Delete
                                        </button>
                                    </td>
                                </tr>)) : <tr className="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                <td scope="row"
                                    className="px-6 py-4 font-medium text-gray-900 dark:text-white whitespace-nowrap">
                                    No tags
                                </td>
                                <td className="px-6 py-4">
                                    No tags
                                </td>
                                <td className="px-6 py-4">
                                    No tags
                                </td>
                                <td className="px-6 py-4 text-right">
                                    <button type="button"
                                            className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Edit
                                    </button>
                                </td>
                                <td className="px-6 py-4 text-right">
                                    <button type="button"
                                            className="font-medium text-blue-600 dark:text-blue-500 hover:underline">Delete
                                    </button>
                                </td>
                            </tr>}
                    </tbody>
                </table>
            </div>
        </Layout>
    )
}


export default Tag