import Layout from "../../../components/layouts/Layout"
import TagForm from "../../../components/forms/TagForm";
import {ChangeEvent, useState} from "react";
import axios from "axios";
import {server} from "../../../config/api-url";

const CreateTag = () => {
    const [tag, setTag] = useState<string>('');

    const handleCreateTag = async (e: ChangeEvent) => {
        e.preventDefault()
        const accessToken = localStorage.getItem('accessToken')
        try {
            const response = await axios.post(`${server}/product/tag/add`, {tag}, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            if (response.status === 200) window.location.href = "/admin/tag"
        } catch (err: any) {
            //todo
            console.log(err.response)
        }
    }

    return (
        <Layout protect={true} description={"This page is where the admin can create a tag."}
                title={"Create tag"}>
            <div><h1 className="text-center text-3xl mt-20">Add a new tag</h1></div>
            <div className="w-9/12 mx-auto mt-32">
                <TagForm action={handleCreateTag} setState={setTag}/>
            </div>
        </Layout>
    )
}

export default CreateTag