import Layout from "../../../../components/layouts/Layout";
import TagForm from "../../../../components/forms/TagForm";
import axios from "axios";
import {server} from "../../../../config/api-url";
import {Tag} from "../../../../interfaces";
import {ChangeEvent, useState} from "react";

const UpdateTag = ({data}: { data: Tag }) => {
    const [tag, setTag] = useState<string>('');

    const handleCreateTag = async (e: ChangeEvent) => {
        e.preventDefault()
        const accessToken = localStorage.getItem('accessToken')
        try {
            const response = await axios.patch(`${server}/product/tag/update/${data.id}`, {tag}, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            if (response.status === 200) window.location.href = "/admin/tag"
        } catch (err: any) {
            //todo
            console.log(err.response)
        }
    }
    return (
        <Layout protect={true} description={"Here the admin can update a tag"}
                title={"Update" + " " + data.tagName}>
            <div><h1 className="text-center text-3xl mt-20">Update a tag</h1></div>
            <div className="w-9/12 mx-auto mt-32">
                <TagForm action={handleCreateTag} setState={setTag} inputValue={data}/>
            </div>
        </Layout>
    )
}


export async function getServerSideProps({req, query}: any) {
    try {
        const response = await axios(`${server}/product/tag/${query.id}`, {
            headers: {
                Authorization: `Bearer ${req.cookies.jwtCookie}`,
            },
        })
        const resProd: Tag = response.data.response

        return {
            props: {data: resProd},
        }
    } catch (err) {
        return {
            props: {data: {}},
        }
    }
}

export default UpdateTag