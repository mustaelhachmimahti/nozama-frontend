import Layout from "../../components/layouts/Layout";
import SignupForm from "../../components/forms/SignupForm";
import {ChangeEvent, useState} from "react";
import {SignupCheck, SignupDto} from "../../interfaces";
import {useRouter} from "next/router";
import axios from "axios";
import {server} from "../../config/api-url";
import {NextPage} from "next";

const Signup: NextPage = () => {
    const router = useRouter();
    const [signup, setSignup] = useState<SignupDto>({
        email: '',
        password: '',
        firstName: '',
        lastName: '',
        address: ''
    });

    const [checkPassword, setCheckPassword] = useState<SignupCheck>({
        confirmPassword: '',
    });

    const [error, setError] = useState<string[]>([]);
    const handleSignup = async (e: ChangeEvent) => {
        e.preventDefault();
        try {
            if (signup.password !== checkPassword.confirmPassword && signup.password.length < 6) {
                setError(["Password must be at least 6 characters!", "Is not the same password!"])
                return;
            }
            if (signup.password.length < 6) {
                setError(["Password must be at least 6 characters!"])
                return;
            }
            if (signup.password !== checkPassword.confirmPassword) {
                setError(["Is not the same password!"])
                return;
            }
            const response = await axios.post(`${server}/auth/signup`, signup);
            if (response) if (response.status === 200) await router.push('/login')
        } catch (err: any) {
            if (err.response)
                if (err.response.status) {
                    if (err.response.status === 403) {
                        const errors: any = [err.response.data.message];
                        setError(errors);
                    }
                    if (err.response.status === 400) {
                        const errors = err.response.data.message.map((e: any) => e);
                        setError(errors);
                    }
                }
        }
    };
    return (
        <Layout protect={false} description={"This is the signup page where you signup to be able to login."}
                title={"Signup"}>
            <h1 className="text-center text-4xl mt-5">Sign up</h1>
            <div className="mt-10"><SignupForm action={handleSignup} error={error} setState={setSignup} state={signup}
                                               check={checkPassword} setStateCheck={setCheckPassword}/></div>
        </Layout>
    )
}


export default Signup