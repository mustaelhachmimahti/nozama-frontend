import axios from "axios";
import Layout from "../../components/layouts/Layout";
import {server} from "../../config/api-url";
import {Product, ProductOneProps, Tag, TagsProductsRelDto} from "../../interfaces";
import moment from "moment";

const OneProduct = ({data, tag}: ProductOneProps) => {
    return (
        <Layout protect={true} description={data.description} title={data.title}>
            <div
                className="p-4 w-full text-center bg-white rounded-lg border shadow-md sm:p-8 dark:bg-gray-800 dark:border-gray-700">
                <h5 className="mb-2 text-3xl font-bold text-gray-900 dark:text-white">{data.title}</h5>
                <div className="flex justify-center">{
                    tag ? tag.map((e: string, i: number) => (
                        <p key={i}
                           className="m-1 bg-gray-200 hover:bg-gray-300 rounded-full px-2 font-bold text-sm leading-loose cursor-pointer">{e}</p>)) : ""
                }</div>
                <div className="justify-around space-y-4 sm:flex sm:space-y-0 sm:space-x-4">
                    <img alt="Placeholder" className="block h-3/6 "
                         src={data.imageLink ? data.imageLink : "https://picsum.photos/600/400/?random"}/>
                    <div>
                        <p className="text-left"><span
                            className="font-bold">Description:</span><br/> {data.description ? data.description : "No description"}
                        </p>
                        <p className="text-left"><span
                            className="font-bold">Price:</span><br/> {data.price ? data.price : "No price"}$
                        </p>
                        <p className="text-left"><span
                            className="font-bold">Stock:</span><br/> {data.stock ? data.stock : "No stock"}
                        </p>
                        <p className="text-left"><span
                            className="font-bold">Stock:</span><br/> {data.createdAt ? moment(data.createdAt).format('MMMM d, YYYY') : "No date"}
                        </p>
                    </div>
                </div>
            </div>
        </Layout>

    )
}

export async function getServerSideProps({query, req}: any) {
    const {id} = query
    try {
        const response = await axios(`${server}/product/${id}`, {
            headers: {
                Authorization: `Bearer ${req.cookies.jwtCookie}`,
            },
        })
        const responseTag = await axios(`${server}/product/tag/all`, {
            headers: {
                Authorization: `Bearer ${req.cookies.jwtCookie}`,
            },
        })
        const tagsProduct: string[] = []
        const resAllTag: Tag[] = responseTag.data.response
        const res: Product = response.data.response
        res.tags.forEach((e: TagsProductsRelDto) => {
            return resAllTag.forEach((el: Tag) => {
                if (+e.tagId === el.id) tagsProduct.push(el.tagName)
            })
        })
        return {
            props: {data: res, tag: tagsProduct},
        }
    } catch (err) {
        return {
            props: {data: {}, tag: []},
        }
    }
}

export default OneProduct