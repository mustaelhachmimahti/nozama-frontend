# Nozama

## Get started

1. `yarn` to install dependencies


2. Exécutez le backend, vous n'avez pas besoin de créer un .env, assurez-vous juste que `SECRET_JWT_TOKEN` dans le
   backend et le frontend est le même.


3. `yarn dev` to run in mode development

### Lien vers l'application déployée:

#### Lorsque vous vous rendez sur le site web et que vous essayez de vous connecter pour la première fois, il se peut qu'il n'y ait aucune réaction pendant 10 à 20 secondes. Cela ne se produit que la première fois, car il s'agit d'un service gratuit.

https://nozama-app.herokuapp.com/


##### Admin credential in deployed page

Admin email: admin@admin.com

Admin password: adminadmin
