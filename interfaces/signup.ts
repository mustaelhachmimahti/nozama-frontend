import {Dispatch, SetStateAction} from 'react';

export interface SignupDto {
    email: string;
    password: string
    firstName: string,
    lastName: string,
    address: string
}

export interface SignupCheck {
    confirmPassword: string
}

type Dispatcher<S> = Dispatch<SetStateAction<S>>;

export interface SignupFormProps {
    state: SignupDto
    setState: Dispatcher<SignupDto>
    action: (e: any) => Promise<any>
    error: string[]
    setStateCheck: Dispatcher<SignupCheck>
    check: SignupCheck
}