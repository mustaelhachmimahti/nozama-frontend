import {Dispatch, SetStateAction} from 'react';

export interface Product {
    id: number;
    title: string;
    description: string;
    imageLink: string;
    price: string;
    stock: number;
    tags: TagsProductsRelDto[]
    createdAt: Date;
    updatedAt: Date;
}

export interface Tag {
    id: number
    tagName: string
    createdAt: Date
    updatedAt: Date
}

export interface ProductTagProps {
    id: number;
    title: string;
    description: string;
    imageLink: string;
    price: string;
    stock: number;
    tags: TagsProductsRelDto[]
    createdAt: Date;
    updatedAt: Date;
}

export interface ProductOneProps {
    data: ProductTagProps
    tag: string[]
}


export interface ProductManyProps {
    res: Product[]
}

export interface TagsProductsRelDto {
    productId: number;
    tagId: string;
    assignedAt: Date;
}


export interface ProductDto {
    title: string;
    description: string;
    imageLink: string;
    price: number;
    stock: number;
}

type Dispatcher<S> = Dispatch<SetStateAction<S>>;

export interface ProductFormProps {
    state: ProductDto
    setState: Dispatcher<ProductDto>
    action: (e: any) => Promise<any>,
    tag: Tag[]
    actionTag: (e: any) => any,
    inputValue?: Product
    error: string[]
}

export interface TagsProps {
    data: Tag[]
}

export interface ResponseUpdateProduct {
    res: Tag[]
    resProd: Product
}

export interface UpdateProductProps {
    data: ResponseUpdateProduct
}