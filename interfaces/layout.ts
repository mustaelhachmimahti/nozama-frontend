import React from "react";

export interface LayoutProps {
    children: React.ReactNode
    protect: boolean
    title: string,
    description: string
}