import {Dispatch, SetStateAction} from "react";
import {Tag} from "./product";

type Dispatcher<S> = Dispatch<SetStateAction<S>>;


export interface TagFormProps {
    setState: Dispatcher<string>
    action: (e: any) => Promise<any>,
    inputValue?: Tag
}