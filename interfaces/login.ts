import {Dispatch, SetStateAction} from 'react';

export interface LoginDto {
    email: string;
    password: string
}

type Dispatcher<S> = Dispatch<SetStateAction<S>>;

export interface LoginFormProps {
    state: LoginDto
    setState: Dispatcher<LoginDto>
    action: (e: any) => Promise<any>,
    error: string[]
}