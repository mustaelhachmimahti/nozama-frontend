export interface Payload {
    sub: number;
    email: string;
    createdAt: Date;
    updatedAt: Date;
    iat: number;
    exp: number;
}

export interface PayloadData {
    payload: Payload;
}

export interface PayloadResponse {
    data: PayloadData;
}
