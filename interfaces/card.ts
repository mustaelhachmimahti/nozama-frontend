export interface CardProps {
    id: number;
    title: string;
    description: string;
    imageLink: string;
    price: string;
    stock: number;
    createdAt: Date;
    updatedAt: Date;
    admin: boolean
    href: string
}
