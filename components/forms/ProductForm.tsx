import {ChangeEvent} from "react";
import {ProductFormProps, Tag} from "../../interfaces";

const ProductForm = ({setState, state, action, tag, actionTag, inputValue, error}: ProductFormProps) => {
    return (
        <>
            {error.length ? error.map((e: string, i: number) => (
                <p key={i} className="text-red-500">{e}</p>)) : null}
            <form onSubmit={action}>
                <div className="mb-6">
                    <label htmlFor="title"
                           className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Title</label>
                    <input type="text" id="title" defaultValue={inputValue ? inputValue.title : ""}
                           className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                           onChange={(e: ChangeEvent<HTMLInputElement>) => {
                               setState({...state, title: e.target.value})
                           }}/>
                </div>
                <div className="mb-6">
                    <label htmlFor="imgLink"
                           className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Image</label>
                    <input type="text" id="imgLink" defaultValue={inputValue ? inputValue.imageLink : ""}
                           className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                           onChange={(e: ChangeEvent<HTMLInputElement>) => {
                               setState({...state, imageLink: e.target.value})
                           }}
                    />
                </div>
                <div className="mb-6">
                    <label htmlFor="price"
                           className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Price</label>
                    <input type="text" id="price" defaultValue={inputValue ? inputValue.price : ""}
                           className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                           onChange={(e: ChangeEvent<HTMLInputElement>) => {
                               setState({...state, price: +e.target.value})
                           }}
                    />
                </div>
                <div className="mb-6">
                    <label htmlFor="stock"
                           className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Stock</label>
                    <input type="text" id="stock" defaultValue={inputValue ? inputValue.stock : ""}
                           className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light"
                           onChange={(e: ChangeEvent<HTMLInputElement>) => {
                               setState({...state, stock: +e.target.value})
                           }}
                    />
                </div>
                <label htmlFor="description"
                       className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400">Your
                    message</label>
                <textarea id="description" rows={4} defaultValue={inputValue ? inputValue.description : ""}
                          className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                          onChange={(e: ChangeEvent<HTMLTextAreaElement>) => {
                              return setState({...state, description: e.target.value});
                          }}
                          placeholder="Your description..."

                />

                {
                    // !inputValue ?
                    <button
                        className="mt-6 block text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                        type="button" data-modal-toggle="defaultModal" //disabled={!!inputValue}
                    >
                        Assign tags
                    </button> //: ""
                }

                <div id="defaultModal" aria-hidden="true"
                     className="fixed top-0 left-0 right-0 z-50 hidden w-full overflow-x-hidden overflow-y-auto md:inset-0 h-modal md:h-full">
                    <div className="relative w-full h-full max-w-2xl p-4 md:h-auto">
                        <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
                            <div
                                className="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                                <h3 className="text-xl font-semibold text-gray-900 dark:text-white">
                                    Tags
                                </h3>
                                <button type="button"
                                        className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                                        data-modal-toggle="defaultModal">
                                    <svg className="w-5 h-5" fill="currentColor" viewBox="0 0 20 20"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fillRule="evenodd"
                                              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                              clipRule="evenodd"/>
                                    </svg>
                                </button>
                            </div>
                            <div className="p-6 space-y-6">
                                {
                                    tag.map((tag: Tag, i: number) => (
                                            <div key={i} className="flex items-center mb-4">
                                                <input id="default-checkbox" type="checkbox" value={tag.id}
                                                       className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                                />
                                                <label htmlFor="default-checkbox"
                                                       className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">{tag.tagName}</label>
                                            </div>
                                        )
                                    )
                                }
                            </div>
                            <div
                                className="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                                <button data-modal-toggle="defaultModal" type="button" onClick={actionTag}
                                        className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                    Accept
                                </button>
                                <button data-modal-toggle="defaultModal" type="button"
                                        className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit"
                        className="mt-6 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                    {inputValue ? "Update product" : "Add product"}
                </button>
            </form>
        </>

    )

}


export default ProductForm