import {LoginFormProps} from "../../interfaces";
import {ChangeEvent, FunctionComponent} from "react";

const LoginForms: FunctionComponent<LoginFormProps> = ({state, setState, action, error}: LoginFormProps) => {

    return (
        <>
            <div
                className="code-preview rounded-xl bg-gradient-to-r bg-white dark:bg-gray-900 border border-gray-200 dark:border-gray-700 p-2 sm:p-6 dark:bg-gray-800 w-3/5 mx-auto">
                <form onSubmit={action}>
                    <div className="mb-6">
                        {error.length ? error.map((e: string, i: number) => (
                            <p key={i} className="text-red-500">{e}</p>)) : null}
                        <label htmlFor="email"
                               className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Your
                            email</label>
                        <input type="email" id="email"
                               className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                               placeholder="name@flowbite.com" onChange={(e: ChangeEvent<HTMLInputElement>) => {
                            setState({...state, email: e.target.value})
                        }}/>
                    </div>
                    <div className="mb-6">
                        <label htmlFor="password"
                               className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Your
                            password</label>
                        <input type="password" id="password"
                               className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                               onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                   setState({...state, password: e.target.value})
                               }}/>
                    </div>

                    <button type="submit"
                            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Login
                    </button>
                </form>
            </div>
        </>
    )

}
export default LoginForms