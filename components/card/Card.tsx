import {CardProps} from "../../interfaces";
import moment from "moment";
import axios from "axios";
import {server} from "../../config/api-url";

const Card = ({title, description, imageLink, createdAt, id, price, stock, admin, href}: CardProps) => {
    const truncate = (str: string, max = 10): string => {
        const array = str.trim().split(' ');
        const ellipsis = array.length > max ? '...' : '';
        return array.slice(0, max).join(' ') + ellipsis;
    };
    const deleteProduct = async (id: number) => {
        const accessToken = localStorage.getItem('accessToken')
        try {
            const response = await axios.delete(`${server}/product/remove/${+id}`, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            })
            console.log(response)
        } catch (err: any) {
            //todo
            console.log(err.response)
        }
    }
    return (

        <div className="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
            <article className="overflow-hidden rounded-lg shadow-lg">
                <a href="#">
                    <img alt="Placeholder" className="block h-auto w-full"
                         src={imageLink}/>
                </a>

                <header
                    className="flex items-center justify-between leading-tight p-2 md:p-4">
                    <h1 className="text-lg">
                        <a className="no-underline hover:underline text-black" href="#">
                            {title ? title : "No title"}
                        </a>
                    </h1>
                    <p className="text-grey-darker text-sm">
                        {createdAt ? moment(createdAt).format('MMMM d, YYYY') : "No date"}
                    </p>
                </header>
                <main className="p-2 md:p-4">
                    <p>{description ? truncate(description, 15) : "No description"}</p>
                    <br/>
                    <div className="flex items-center justify-between">
                        <p>{price ? price + "$" : "No price"}</p>
                        <p>Stock: {stock && stock > 0 ?
                            <i className="fa-solid fa-check"/> :
                            <i className="fa-solid fa-xmark"/>
                        }</p>
                    </div>
                </main>
                <footer
                    className="flex items-center justify-between leading-none p-2 md:p-4">
                    <a href={href + id}
                       className="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        Read more
                        <svg className="ml-2 -mr-1 w-4 h-4" fill="currentColor"
                             viewBox="0 0 20 20"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd"
                                  d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                                  clipRule="evenodd"/>
                        </svg>
                    </a>
                    <div className="flex">
                        {
                            admin ? <button onClick={() => deleteProduct(id)}
                                            className="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            >Delete
                            </button> : ""
                        }
                        {
                            admin ? <a href={"/admin/product/update/" + id}
                                       className="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            >Update
                            </a> : ""
                        }</div>
                </footer>
            </article>
        </div>
    )
}

export default Card