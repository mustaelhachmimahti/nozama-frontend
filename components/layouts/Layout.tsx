import Head from "next/head";
import Footer from "../footers/Footer";
import {LayoutProps} from "../../interfaces";
import {FunctionComponent} from "react";
import NoAuthHeader from "../headers/NoAuthHeader";
import AuthHeader from "../headers/AuthHeader";

const Layout: FunctionComponent<LayoutProps> = ({children, protect, title, description}: LayoutProps) => {
    return (
        <>
            <Head>
                <title>{title}</title>
                <meta name="description" content={description}/>
                <link rel="icon" href="/favicon.ico"/>
                <link
                    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
                    rel="stylesheet"
                />
                <link rel="stylesheet"
                      href="https://fonts.googleapis.com/css?family=Bangers"/>
                {/* eslint-disable-next-line @next/next/no-sync-scripts */}
                <script src="https://unpkg.com/flowbite@1.4.3/dist/flowbite.js"/>
            </Head>
            <div className="all">
                {protect ? <AuthHeader/> : <NoAuthHeader/>}
                <main className="my-6">{children}</main>
                <Footer/>
            </div>
        </>
    )
}

export default Layout