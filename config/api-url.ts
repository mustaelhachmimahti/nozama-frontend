const dev: boolean = process.env.NODE_ENV !== 'production';

export const server: string = dev
    ? 'http://localhost:3001'
    : 'https://nozama-backend-app.herokuapp.com';


export const routes: string = dev ? 'http://localhost:3000'
    : 'https://nozama-app.herokuapp.com';